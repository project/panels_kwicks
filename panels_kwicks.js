
(function ($) {
  Drupal.behaviors.panelsKwicks = {
    attach: function (context, settings) {
      if (settings.panels_kwicks.regions) {
        $.each(settings.panels_kwicks.regions, function(key, instance) {
          $(key + " .kwicks").kwicks(instance);
        });
      }
    }
  };
}(jQuery));
