
== Installation ==

You must install the packed version of the kwicks 1.5.1 library. You can get it here:
http://www.jeremymartin.name/projects.php?project=kwicks

It should be placed somewhere like:
  sites/all/libraries/kwicks
    - or -
  sites/default/libraries/kwicks
such that you end up with a path like:
  sites/all/libraries/kwicks/jquery.kwicks-1.5.1.pack.js
    - or -
  sites/default/libraries/kwicks/jquery.kwicks-1.5.1.pack.js

Now you just need to enable this module.

== Configuration ==

If you go to one of your panels, you should now have a 'kwicks' style option for panel regions.
