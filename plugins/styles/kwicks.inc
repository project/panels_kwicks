<?php

/**
 * @file
 * Definition of the 'kwicks' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Kwicks'),
  'description' => t('Presents the panes in the form of Kwiks panes.'),
  'render region' => 'panels_kwicks_kwicks_style_render_region',
  'settings form' => 'panels_kwicks_kwicks_style_settings_form',
  'settings validate' => 'panels_kwicks_kwicks_style_settings_form_validate',
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_kwicks_kwicks_style_render_region($vars) {
  if ($kwicks_dir = libraries_get_path('kwicks')) {
    $display = $vars['display'];
    $region_id = $vars['region_id'];
    $panes = $vars['panes'];
    $settings = $vars['settings'];
    
    $items = array();
    
    foreach ($panes as $pane_id => $item) {
      $items[] = $item;
    }
    
    drupal_add_js($kwicks_dir . '/' . (variable_get('panels_kwicks_debug', FALSE) ? PANELS_KWICKS_JS : PANELS_KWICKS_JS_PACKED));
    drupal_add_js(drupal_get_path('module', 'panels_kwicks') . '/panels_kwicks.js');
    
    panels_kwicks_add_js($display, $settings);
    panels_kwicks_add_css($display, $settings);
    
    return '<div class="panels-kwicks">' . theme('item_list', array('items' => $items, 'type' => 'ul', 'attributes' => array('class' => array('kwicks')))) . '</div>';
  }
}

/**
 * Settings form callback.
 */
function panels_kwicks_kwicks_style_settings_form($style_settings) {
  $form['kwicks_pane_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Pane Width'),
    '#description' => t('The initial width of a kwick pane.'),
    '#default_value' => (isset($style_settings['kwicks_pane_width'])) ? $style_settings['kwicks_pane_width'] : '200',
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['kwicks_pane_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Pane Height'),
    '#description' => t('The initial height of a kwick pane.'),
    '#default_value' => (isset($style_settings['kwicks_pane_height'])) ? $style_settings['kwicks_pane_height'] : '300',
    '#element_validate' => array('element_validate_integer_positive'),
  );
  
  $form['kwicks_minmax'] = array(
    '#type' => 'select',
    '#title' => t('Min or Max'),
    '#description' => t('Which sizing measurement should we use.? We can not use both.'),
    '#options' => array(
      'min' => t('Min'),
      'max' => t('Max'),
    ),
    '#default_value' => (isset($style_settings['kwicks_minmax'])) ? $style_settings['kwicks_minmax'] : 'min',
  );
  $form['kwicks_min_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Min Size'),
    '#description' => t('The width or height (in pixels) of a fully collapsed kwick pane. If vertical is set to true, then min refers to the height - otherwise it is the width.'),
    '#default_value' => (isset($style_settings['kwicks_min_value'])) ? $style_settings['kwicks_min_value'] : '100',
    '#element_validate' => array('element_validate_integer'),
  );
  $form['kwicks_max_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Size'),
    '#description' => t('The width or height (in pixels) of a fully expanded kwick pane. If vertical is set to true, then max refers to the height - otherwise it is the width.'),
    '#default_value' => (isset($style_settings['kwicks_max_value'])) ? $style_settings['kwicks_max_value'] : '400',
    '#element_validate' => array('element_validate_integer'),
  );
  
  $form['kwicks_vertical'] = array(
    '#type' => 'checkbox',
    '#title' => t('Vertical Panes'),
    '#default_value' => (isset($style_settings['kwicks_vertical'])) ? $style_settings['kwicks_vertical'] : FALSE,
  );
  
  $form['kwicks_sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sticky Panes'),
    '#description' => t('One kwicks pane will always be expanded if true.'),
    '#default_value' => (isset($style_settings['kwicks_sticky'])) ? $style_settings['kwicks_sticky'] : FALSE,
  );
  
  $form['kwicks_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Easing Duration'),
    '#description' => t('The number of milliseconds required for each animation to complete.'),
    '#default_value' => (isset($style_settings['kwicks_duration'])) ? $style_settings['kwicks_duration'] : '200',
    '#element_validate' => array('element_validate_integer_positive'),
  );
  
  $form['kwicks_spacing'] = array(
    '#type' => 'textfield',
    '#title' => t('Pane Spacing'),
    '#description' => t('The width (in pixels) separating each kwick pane.'),
    '#default_value' => (isset($style_settings['kwicks_spacing'])) ? $style_settings['kwicks_spacing'] : '0',
    '#element_validate' => array('element_validate_integer'),
  );
  
  return $form;
}

function panels_kwicks_add_js($display, $settings = array()) {
  static $displays_used = array();
  $idstr = empty($display->css_id) ? '.panels-kwicks' : "#$display->css_id";
  
  if (empty($displays_used[$idstr])) {
    $displays_used[$idstr] = TRUE;
    
    $instance = array();
    
    // Handle min/max settings
    $minmax = 'min';
    if (isset($settings['kwicks_minmax'])) {
      $minmax = $settings['kwicks_minmax'];
    }
    if ($minmax = 'max') {
      if (isset($settings['kwicks_max_value'])) {
        $instance['max'] = intval($settings['kwicks_max_value']);
      }
      else {
        $instance['max'] = 400;
      }
    }
    else {
      if (isset($settings['kwicks_min_value'])) {
        $instance['min'] = intval($settings['kwicks_min_value']);
      }
      else {
        $instance['min'] = 100;
      }
    }
    
    if (isset($settings['kwicks_vertical'])) {
      $instance['isVertical'] = $settings['kwicks_vertical'] == TRUE;
    }
    if (isset($settings['kwicks_sticky'])) {
      $instance['sticky'] = $settings['kwicks_sticky'] == TRUE;
    }
    if (isset($settings['kwicks_duration'])) {
      $instance['duration'] = intval($settings['kwicks_duration']);
    }
    if (isset($settings['kwicks_spacing'])) {
      $instance['spacing'] = intval($settings['kwicks_spacing']);
    }
    
    drupal_add_js(array(
      'panels_kwicks' => array(
        'regions' => array(
          $idstr => $instance,
        ),
      ),
    ), 'setting');
  }
}

function panels_kwicks_add_css($display, $settings = array()) {
  static $displays_used = array();
  
  if (empty($displays_used[$display->css_id])) {
    panels_kwicks_css($display, $settings);
    $displays_used[$display->css_id] = TRUE;
  }
}

/**
 * Generates the dynamic CSS.
 *
 * @param $display
 *   A Panels display object.
 */
function panels_kwicks_css($display, $settings) {
  $idstr = empty($display->css_id) ? '.panels-kwicks' : "#$display->css_id";
  $css_id = 'panels-kwicks:' . $idstr;

  ctools_include('css');
  $filename = ctools_css_retrieve($css_id);
  if (!$filename) {
    $filename = ctools_css_store($css_id, _panels_kwicks_css($idstr, $settings), FALSE);
  }

  drupal_add_css($filename, array('preprocess' => FALSE));
}

/**
 * Generates the dynamic CSS.
 */
function _panels_kwicks_css($idstr, $settings) {
  $url = base_path() . drupal_get_path('module', 'panels_kwicks');
  
  $width  = isset($settings['kwicks_pane_width']) ? $settings['kwicks_pane_width'] : 200;
  $height = isset($settings['kwicks_pane_height']) ? $settings['kwicks_pane_height'] : 100;
  
  $css = <<<EOF
  
  $idstr .kwicks {
    list-style: none;
    position: relative;
    margin: 0;
    padding: 0;
  }  
  $idstr .kwicks li{
    display: block;
    overflow: hidden;
    padding: 0;
    cursor: pointer;
  }
  
  $idstr .kwicks li {
    float: left;
    width: {$width}px;
    height: {$height}px;
    margin-right: 5px;
  }
  
EOF;
  
  return $css;
}
